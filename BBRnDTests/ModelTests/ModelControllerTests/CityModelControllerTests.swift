//
//  CityModelControllerTests.swift
//  BBRnDTests
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import XCTest
@testable import BBRnD

class CityModelControllerTests: XCTestCase {
    
    private var sut : CityModelController!
    
    override func setUp() {
        super.setUp()
        
        sut = CityModelController()
    }
    
    func test_loadContentAsList_whenCalled_didReturnData() {
        // Arrange
        
        // Act
        let list = sut.loadContentAsList(fromJSONFile: "cities_test")
        
        // Assert
        XCTAssert(list != nil)
    }
    
    func test_loadContentAsList_didReturnData_isValidList() {
        // Arrange
        
        // Act
        let list = sut.loadContentAsList(fromJSONFile: "cities_test")
        
        // Assert
        XCTAssert(list!.count > 0)
    }
}
