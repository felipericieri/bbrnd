//
//  CityTests.swift
//  BBRnDTests
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import XCTest
@testable import BBRnD

class CityTests: XCTestCase {
    
    private var sut : City!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func test_init_initializedWithJSONObject_didCreateInstance() {
        // Arrange
        let id = 707860
        let name = "Hurzuf"
        let country = "UA"
        let lat = 44.549999
        let lon = 34.283333
        let object : [String : Any] = [
            "country": country,
            "name": name,
            "_id": id,
            "coord": [
                "lon": lon,
                "lat": lat
            ]
        ]
        
        // Act
        let sut = City(json: object)
        
        // Assert
        XCTAssert(sut.id == id)
        XCTAssert(sut.name == name)
        XCTAssert(sut.country == country)
        XCTAssert(sut.latitude == lat)
        XCTAssert(sut.longintude == lon)
    }
}
