//
//  CityPinTests.swift
//  BBRnDTests
//
//  Created by Felipe Ricieri on 18/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import XCTest
@testable import BBRnD

class CityPinTests: XCTestCase {
    
    private var sut : CityPin!
    
    override func setUp() {
        super.setUp()
    }
    
    func test_init_withCity_gotValidCoordinates() {
        // Arrange
        let id = 707860
        let name = "Hurzuf"
        let country = "UA"
        let lat = 44.549999
        let lon = 34.283333
        let object : [String : Any] = [
            "country": country,
            "name": name,
            "_id": id,
            "coord": [
                "lon": lon,
                "lat": lat
            ]
        ]
        let city = City(json: object)
        
        // Act
        sut = CityPin(city: city)
        
        // Assert
        XCTAssert(sut.coordinate.latitude == city.latitude)
        XCTAssert(sut.coordinate.longitude == city.longintude)
    }
}
