//
//  MainStoryboardTests.swift
//  BBRnDTests
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import XCTest
@testable import BBRnD

class MainStoryboardTests: XCTestCase {
    
    private var sut : MainStoryboard!
    
    override func setUp() {
        super.setUp()
        
        sut = MainStoryboard()
    }
    
    func test_citiesListController_onLoad_didReturnViewController() {
        // Arrange
        
        // Act
        let vc = sut.citiesListController()
        
        // Assert
        XCTAssert(vc is CitiesListViewController)
    }
    
    func test_cityMapController_onLoad_didReturnViewController() {
        // Arrange
        
        // Act
        let vc = sut.cityMapController()
        
        // Assert
        XCTAssert(vc is CityMapViewController)
    }
}
