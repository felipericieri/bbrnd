//
//  CitiesListViewModelTests.swift
//  BBRnDTests
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import XCTest
@testable import BBRnD

class CitiesListViewModelTests: XCTestCase {
    
    private var sut : CitiesListViewModel!
    
    override func setUp() {
        super.setUp()
        
        let modelController = CityModelController()
        sut = CitiesListViewModel(modelController: modelController)
    }
    
    func test_loadCitiesList_onLoad_isValidList() {
        // Arrange
        let expect = expectation(description: "Application did finish loading city list.")
        let sub = NotificationCenter.default.addObserver(forName: CitiesListViewModel.DidFinishLoadingDataNotification, object: nil, queue: nil) { _ in
            XCTAssert(self.sut.numberOfRows > 0)
            expect.fulfill()
        }
        
        // Act
        sut.loadCitiesList(fileName: "cities_test")
        
        // Assert
        waitForExpectations(timeout: 5, handler: nil)
        NotificationCenter.default.removeObserver(sub)
    }
    
    func test_loadCitiesList_onLoad_didFinishLoadingDataNotificationIsFired() {
        // Arrange
        let expect = expectation(description: "Application did finish loading city list.")
        let sub = NotificationCenter.default.addObserver(forName: CitiesListViewModel.DidFinishLoadingDataNotification, object: nil, queue: nil) { _ in
            expect.fulfill()
        }
        
        // Act
        sut.loadCitiesList(fileName: "cities_test")
        
        // Assert
        waitForExpectations(timeout: 5, handler: nil)
        NotificationCenter.default.removeObserver(sub)
    }
    
    func test_searchForPrefix_userDidType_foundResults() {
        // Arrange
        let expect = expectation(description: "Application did finish loading city list.")
        let not = NotificationCenter.default
        let sub = not.addObserver(forName: CitiesListViewModel.DidFinishLoadingDataNotification, object: nil, queue: nil) { _ in
            self.sut.startSearching()
            self.sut.search(forPrefix: "Am")
        }
        let sub2 = not.addObserver(forName: CitiesListViewModel.DidFinishSearchingNotification, object: nil, queue: nil) { _ in
            XCTAssert(self.sut.numberOfRows == 6)
            expect.fulfill()
        }
        
        // Act
        sut.loadCitiesList(fileName: "cities_test")
        
        // Assert
        waitForExpectations(timeout: 5, handler: nil)
        NotificationCenter.default.removeObserver(sub)
        NotificationCenter.default.removeObserver(sub2)
    }
    
    func test_searchForPrefix_userDidType_foundNoResults() {
        // Arrange
        let expect = expectation(description: "Application did finish loading city list.")
        let not = NotificationCenter.default
        let sub = not.addObserver(forName: CitiesListViewModel.DidFinishLoadingDataNotification, object: nil, queue: nil) { _ in
            self.sut.startSearching()
            self.sut.search(forPrefix: "Berlin")
        }
        let sub2 = not.addObserver(forName: CitiesListViewModel.DidFinishSearchingNotification, object: nil, queue: nil) { _ in
            XCTAssert(self.sut.numberOfRows == 0)
            expect.fulfill()
        }
        
        // Act
        sut.loadCitiesList(fileName: "cities_test")
        
        // Assert
        waitForExpectations(timeout: 5, handler: nil)
        NotificationCenter.default.removeObserver(sub)
        NotificationCenter.default.removeObserver(sub2)
    }
    
    func test_startSearching() {
        // Arrange
        
        // Act
        sut.startSearching()
        
        // Assert
        XCTAssert(sut.isSearching)
    }
    
    func test_stopSearching() {
        // Arrange
        
        // Act
        sut.stopSearching()
        
        // Assert
        XCTAssertFalse(sut.isSearching)
    }
}
