//
//  CityModelController.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import Foundation

class CityModelController {
    
    /**
     * Load & Serialize data
     */
    func loadContentAsList(fromJSONFile fileName: String) -> [City]? {
        
        // Loads the file first
        guard
            let list = loadContent(fromJSONFile: fileName) as? [[String : Any]]
            else { return nil }
        
        // Then itinerate it in city array
        var citiesList = [City]()
        for object in list {
            let city = City(json: object)
            citiesList.append(city)
        }
        
        return citiesList
    }
    
    /**
     * Load JSON file
     */
    private func loadContent(fromJSONFile fileName: String) -> Any? {
        
        // Load file from Bundle
        guard
            let path = Bundle.main.path(forResource: fileName, ofType: "json")
            else { return nil }
        
        let urlPath = URL(fileURLWithPath: path)
        
        do {
            // Try to get its data
            let data = try Data(contentsOf: urlPath, options: .mappedIfSafe)
            // Try to serialize it
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            // Return the result (may be nil)
            return jsonResult
        } catch let error {
            print("Failed Loading Data from \(fileName).json: \(error.localizedDescription)")
            return nil
        }
    }
}

