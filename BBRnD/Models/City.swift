//
//  City.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import Foundation
import CoreLocation

struct City {
    
    /**
     * City ID
     */
    var id : Int!
    
    /**
     * City Name
     */
    var name : String = ""
    
    /**
     * Country Name
     */
    var country : String = ""
    
    /**
     * Latitude Coordinate
     */
    var latitude : CLLocationDegrees?
    
    /**
     * Longitude Coordinate
     */
    var longintude : CLLocationDegrees?
    
    /**
     * Convenient coordinate property generated based in coord dictionary
     */
    var coordinates : CLLocationCoordinate2D? {
        guard
            let lat = latitude,
            let lng = longintude
            else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
    init(json: [String: Any]) {
        
        self.id = json["_id"] as! Int
        self.name = json["name"] as? String ?? ""
        self.country = json["country"] as? String ?? ""
        
        if  let coord = json["coord"] as? [String : Any] {
            self.latitude = coord["lat"] as? CLLocationDegrees
            self.longintude = coord["lon"] as? CLLocationDegrees
        }
    }
}

