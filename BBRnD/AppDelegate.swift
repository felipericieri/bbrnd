//
//  AppDelegate.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Injecting the desired ViewModel instance to Cities List
        if  let nav = window?.rootViewController as? UINavigationController,
            let vc = nav.viewControllers.first as? CitiesListViewController {
            let modelController = CityModelController()
            vc.viewModel = CitiesListViewModel(modelController: modelController)
        }
        
        return true
    }
}

