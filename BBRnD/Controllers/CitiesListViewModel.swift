//
//  CitiesListViewModel.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import Foundation

class CitiesListViewModel {
    
    /**
     * Cities List source
     */
    fileprivate var source : [City] = []  { didSet {
        print("Cities List was updated!")
        }}
    
    /**
     * Filtered source
     */
    fileprivate var filteredSource : [City] = []
    
    /**
     * Tells either if we should use filtered or original source
     */
    fileprivate(set) var isSearching : Bool = false
    
    /**
     * Responsible for bringing data into the viewModel
     */
    private let modelController : CityModelController
    
    init(modelController: CityModelController) {
        self.modelController = modelController
    }
    
    /**
     * Loads List from file and fill ViewController source
     */
    func loadCitiesList(fileName: String) {
        
        // Sending the dirty job to a background queue
        DispatchQueue.global(qos: .userInitiated).async {
            guard
                let originalList = self.modelController.loadContentAsList(fromJSONFile: fileName)
                else { print("CitiesListViewModel - Failed loading cities list."); return }
            // Sort list
            let sortedList = originalList.sorted { $0.name < $1.name }
            // Coming back to Main queue
            DispatchQueue.main.async {
                self.source = sortedList
                NotificationCenter.default
                    .post(name: CitiesListViewModel.DidFinishLoadingDataNotification, object: nil)
            }
        }
    }
    
    /**
     * Search for Prefix in original source & fill the filtered source
     */
    func search(forPrefix prefix: String) {
        
        print("Looking for \(prefix)...")
        
        // Clear last search
        filteredSource = []
        
        // if prefix is empty, present the original source & return
        guard prefix != "" else {
            filteredSource = source
            NotificationCenter.default
                .post(name: CitiesListViewModel.DidFinishSearchingNotification, object: nil)
            return
        }
        
        // Send dirty job to background queue
        DispatchQueue.global(qos: .userInteractive).async {
            for city in self.source {
                if  city.name.hasPrefix(prefix) {
                    self.filteredSource.append(city)
                }
            }
            print("We found \(self.filteredSource.count) results for \"\(prefix)\"")
            // Now we come back to main queue
            DispatchQueue.main.async {
                NotificationCenter.default
                    .post(name: CitiesListViewModel.DidFinishSearchingNotification, object: nil)
            }
        }
    }
}

// MARK: - 👨🏻‍💻 Exposing Properties for ViewController
extension CitiesListViewModel {
    
    /**
     * The current number of sections ViewController can expect
     */
    var numberOfSections : Int {
        return 1
    }
    
    /**
     * The current number of rows ViewController can expect
     */
    var numberOfRows : Int {
        return isSearching ? filteredSource.count : source.count
    }
    
    /**
     * Returns the object at a given index
     */
    func object(at index: Int) -> City {
        if  isSearching {
            assert(index < filteredSource.count, "Tried to access an index of \(index) out of city (filtered) list boundaries (\(filteredSource.count)).")
            return filteredSource[index]
        } else {
            assert(index < source.count, "Tried to access an index of \(index) out of city list boundaries (\(source.count)).")
            return source[index]
        }
    }
    
    /**
     * Turn searching state on
     */
    func startSearching() {
        guard !isSearching else { return }
        isSearching = true
        print("Search mode: ON")
    }
    
    /**
     * Turn searching state off
     */
    func stopSearching() {
        guard isSearching else { return }
        isSearching = false
        print("Search mode: OFF")
    }
}

// MARK: - 🚀 Available Notifications
extension CitiesListViewModel {
    
    /**
     * Notification to tell ViewController the data is ready
     */
    static let DidFinishLoadingDataNotification = Notification.Name(rawValue: "did_finish_loading_data")
    
    /**
     * Notification to tell ViewController the search is done
     */
    static let DidFinishSearchingNotification = Notification.Name(rawValue: "did_finish_searching")
}


