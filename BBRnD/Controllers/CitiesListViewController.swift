//
//  CitiesListViewController.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import UIKit

final class CitiesListViewController : UIViewController {
    
    /**
     * ViewController's ViewModel
     */
    var viewModel : CitiesListViewModel!
    
    @IBOutlet private weak var table : UITableView!
    @IBOutlet private weak var searchBar : UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting outlets
        table.delegate = self
        table.dataSource = self
        table.keyboardDismissMode = .onDrag
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 55
        searchBar.delegate = self
        
        // Load list
        viewModel.loadCitiesList(fileName: "cities_test")
        
        // Notification Center
        NotificationCenter.default.addObserver(self,
            selector: #selector(CitiesListViewController.notificationViewModelUpdatedData),
            name: CitiesListViewModel.DidFinishLoadingDataNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(CitiesListViewController.notificationViewModelUpdatedData),
            name: CitiesListViewModel.DidFinishSearchingNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(CitiesListViewController.notificationKeyboardDidAppear),
            name: .UIKeyboardWillShow,
            object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(CitiesListViewController.notificationKeyboardDidDisappear),
            name: .UIKeyboardWillHide,
            object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let vc = segue.destination as? CityMapViewController {
            let city = sender as! City
            vc.viewModel = CityMapViewModel(city: city)
        }
    }
    
    /**
     * Fired when ViewModel is done loading data
     */
    @objc private func notificationViewModelUpdatedData() {
        table.reloadData()
    }
    
    /**
     * Triggered when keyboard appears
     */
    @objc private func notificationKeyboardDidAppear() {
        viewModel.startSearching()
    }
    
    /**
     * Triggered when keyboard disappears
     */
    @objc private func notificationKeyboardDidDisappear() {
        viewModel.stopSearching()
    }
}

// MARK: - 👨🏻‍💻 Table Delegate & Data Source
extension CitiesListViewController :
    UITableViewDelegate,
    UITableViewDataSource {
    
    // Rows
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = viewModel.object(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: CityCell.cellIdentifier, for: indexPath) as! CityCell
        cell.viewModel = CityCell.ViewModel(city: object)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = viewModel.object(at: indexPath.row)
        performSegue(withIdentifier: MainStoryboard.Segue.toMap, sender: object)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    // Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
}

// MARK: - 👨🏻‍💻 Search Bar Delegate
extension CitiesListViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.startSearching()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        viewModel.stopSearching()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.search(forPrefix: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Search Cancel button clicked")
        viewModel.stopSearching()
        searchBar.text = nil
        searchBar.resignFirstResponder()
        table.reloadData()
    }
}

