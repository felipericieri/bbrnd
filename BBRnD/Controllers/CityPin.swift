//
//  CityPin.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 18/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import Foundation
import MapKit

class CityPin: NSObject, MKAnnotation {
    
    /**
     * City to be pinned
     */
    let city : City!
    
    /**
     * MKAnnotation coordinates
     */
    var coordinate: CLLocationCoordinate2D {
        assert(city.coordinates != nil, "Tried to access City's coordinates, but there is no valid data.")
        return city.coordinates!
    }
    
    init(city: City) {
        self.city = city
    }
}
