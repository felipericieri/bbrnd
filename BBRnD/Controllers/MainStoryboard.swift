//
//  MainStoryboard.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import UIKit

class MainStoryboard {
    
    private var storyboard : UIStoryboard!
    
    // Better API to reference segues
    struct Segue {
        static let toMap = "toMap"
    }
    
    init() {
        storyboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
    /**
     * Returns UIViewController instance from Main Storyboard with "citiesListController" identifier
     */
    func citiesListController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "citiesListController")
    }
    
    /**
     * Returns UIViewController instance from Main Storyboard with "cityMapController" identifier
     */
    func cityMapController() -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "cityMapController")
    }
}
