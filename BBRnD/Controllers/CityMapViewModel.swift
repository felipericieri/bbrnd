//
//  CityMapViewModel.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 18/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import Foundation
import MapKit

class CityMapViewModel {
    
    /**
     * Current City
     */
    let city : City
    
    /**
     * Pin with City coordinates
     */
    let pin : CityPin
    
    /**
     * Region radius
     */
    let regionRadius : CLLocationDistance = 1000
    
    /**
     * Map Coordinate Region
     */
    var coordinateRegion : MKCoordinateRegion {
        return MKCoordinateRegionMakeWithDistance(city.coordinates!, regionRadius, regionRadius)
    }
    
    init(city: City) {
        self.city = city
        self.pin = CityPin(city: city)
    }
}
