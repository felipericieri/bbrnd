//
//  CityCell.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import UIKit

class CityCell : UITableViewCell {
    
    /**
     * Storyboard identifier
     */
    static let cellIdentifier = "cityCell"
    
    /**
     * Cell's ViewModel
     */
    var viewModel : ViewModel? { didSet {
        guard let vm = viewModel else { return }
        cityNameLabel.text = "\(vm.city.name), \(vm.city.country)"
        }}
    
    @IBOutlet private weak var cityNameLabel : UILabel!
}

extension CityCell {
    struct ViewModel {
        let city : City
    }
}
