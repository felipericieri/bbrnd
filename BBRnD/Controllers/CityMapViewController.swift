//
//  CityMapViewController.swift
//  BBRnD
//
//  Created by Felipe Ricieri on 17/02/2018.
//  Copyright © 2018 Ricieri ME. All rights reserved.
//

import UIKit
import MapKit

final class CityMapViewController : UIViewController {
    
    /**
     * ViewController's ViewModel
     */
    var viewModel : CityMapViewModel!
    
    @IBOutlet private weak var map : MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting outlets
        map.delegate = self
        // Add Annotation in Map
        map.addAnnotation(viewModel.pin)
        // Set Map Center
        map.setRegion(viewModel.coordinateRegion, animated: true)
    }
}

// MARK: - 👨🏻‍💻 Map Delegate
extension CityMapViewController : MKMapViewDelegate {
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        // Select Annotation animated when map is ready
        map.selectAnnotation(viewModel.pin, animated: true)
    }
}


